<?php

namespace App\Http\Livewire\Ingresar;

use App\Models\Car;
use App\Models\Detail;
use App\Models\Registration;
use Livewire\Component;

class Ingresar extends Component
{
    public $searchResult = [];
    public $search = '';

    public $num_placa;
    public $num_serie;
    public $num_vin;
    public $num_motor;
    public $color;
    public $car_id;
    public $placa_anterior;
    public $placa_vigente;
    public $estado;
    public $sede;
    public $propietario;
    public $tarjeta;

    public $description_incidencia;
    public $incidenciaResult;


    public $autos;
    public $marca;
    public $modelos;

    public function mount()
    {

        $this->modelos = collect();
    }

    public function render()
    {
        if (strlen($this->num_placa) > 2) {
            $this->searchResult = Registration::where('num_placa', 'like', '%' . $this->num_placa . '%')
                ->get()
                ->toArray();
        }

        $this->autos = Car::select('marca')
            ->orderBy('marca', 'asc')
            ->groupBy('marca')
            ->get();

        if (!empty($this->marca)) {
            $this->modelos = Car::select('id', 'modelo')
                ->where('marca', $this->marca)
                ->orderBy('modelo', 'asc')
                ->get();
        }

        return view('livewire.ingresar.ingresar');
    }

    public function store()
    {
        Registration::updateOrCreate(
            ['num_placa' => $this->num_placa],
            [
                'num_placa' => $this->num_placa,
                'num_serie' => $this->num_serie,
                'num_vin' => $this->num_vin,
                'num_motor' => $this->num_motor,
                'color' => $this->color,
                'car_id' => $this->car_id,
                'placa_anterior' => $this->placa_anterior,
                'placa_vigente' => $this->placa_vigente,
                'estado' => $this->estado,
                'sede' => $this->sede,
                'propietario' => $this->propietario,
                'tarjeta' => $this->tarjeta
            ]
        );
    }

    public function getData($id)
    {
        $registration = Registration::find($id);
        $car = Car::find($registration['car_id']);

        $this->num_placa = $registration['num_placa'];
        $this->num_serie = $registration['num_serie'];
        $this->num_vin = $registration['num_vin'];
        $this->num_motor = $registration['num_motor'];
        $this->color = $registration['color'];
        $this->marca = $car['marca'];
        $this->car_id = $registration['car_id'];
        $this->placa_anterior = $registration['placa_anterior'];
        $this->placa_vigente = $registration['placa_vigente'];
        $this->estado = $registration['estado'];
        $this->sede = $registration['sede'];
        $this->propietario = $registration['propietario'];
        $this->tarjeta = $registration['tarjeta'];

        $this->incidenciaResult = Detail::where('registration_id', $id)
            ->orderBy('created_at', 'desc')
            ->get()
            ->toArray();
    }

    public function storeModal()
    {
        $this->store();
        $registration = Registration::firstWhere('num_placa', $this->num_placa);

        Detail::create([
            'description' => $this->description_incidencia,
            'registration_id' => $registration['id'],
        ]);

        $this->getData($registration['id']);



        $this->emit('closeModal'); // Close model to using to jquery
    }
}
