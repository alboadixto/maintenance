<?php

namespace App\Http\Livewire\Consultar;

use Livewire\Component;

class Consultar extends Component
{
    public function render()
    {
        return view('livewire.consultar.consultar');
    }
}
