<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use HasFactory;

    //Relación uno a muchos
    public function registrations()
    {
        return $this->hasMany('App\Models\Registration');
    }
}
