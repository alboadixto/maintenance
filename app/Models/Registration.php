<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Registration extends Model
{
    use HasFactory;

    protected $fillable = [
        'num_placa',
        'num_serie',
        'num_vin',
        'num_motor',
        'color',
        'car_id',
        'placa_anterior',
        'placa_vigente',
        'estado',
        'sede',
        'propietario',
        'tarjeta',
    ];
    protected $guarded  = ['id'];

    //Relación uno a muchos
    public function details()
    {
        return $this->hasMany('App\Models\Detail');
    }

    //Relación uno a muchos (inversa)
    public function car()
    {
        return $this->belongsTo('App\Models\Car');
    }
}
