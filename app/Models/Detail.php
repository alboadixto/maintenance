<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    use HasFactory;

    protected $fillable = [
        'description',
        'registration_id',
    ];
    protected $guarded  = ['id'];

    //Relación uno a muchos (inversa)
    public function registration()
    {
        return $this->belongsTo('App\Models\Registration');
    }
}
