<div>
    <div class="row">
        <div class="col-md-12">

            <form autocomplete="off" class="form-horizontal">

                <div class="card ">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title">{{ __('Registro Vehicular') }}</h4>
                    </div>

                    <div class="card-body ">

                        <div class="row">
                            <label class="col-sm-2 col-form-label">{{ __('N° de PLACA') }}</label>
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <input class="form-control" type="text" placeholder="{{ __('N° de placa') }}" required="true" aria-required="true" wire:model.debounce.500ms="num_placa"/>

                                    @if (strlen($num_placa) > 2)
                                        @forelse ($searchResult as $result)
                                        <nav aria-label="breadcrumb" role="navigation">
                                            <ol class="breadcrumb">
                                              <li class="breadcrumb-item active" aria-current="page"><a class="nav-link active" wire:click="getData({{$result['id']}})">{{$result['num_placa']}}</a></li>
                                            </ol>
                                        </nav>
                                        @empty
                                        <nav aria-label="breadcrumb" role="navigation">
                                            <ol class="breadcrumb">
                                              <li class="breadcrumb-item active" aria-current="page"><a class="nav-link active">Sin resultados para {{$num_placa}}</a></li>
                                            </ol>
                                        </nav>
                                        @endforelse
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-sm-2 col-form-label">{{ __('N° de SERIE') }}</label>
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <input class="form-control" name="name" id="input-name" type="text" placeholder="{{ __('N° de serie') }}" required="true" aria-required="true" wire:model="num_serie"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">{{ __('N° de VIN') }}</label>
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <input class="form-control" name="name" id="input-name" type="text" placeholder="{{ __('N° de VIN') }}" required="true" aria-required="true" wire:model="num_vin"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">{{ __('N° de MOTOR') }}</label>
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <input class="form-control" name="name" id="input-name" type="text" placeholder="{{ __('N° de VIN') }}" required="true" aria-required="true" wire:model="num_motor"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">{{ __('COLOR') }}</label>
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <input class="form-control" name="name" id="input-name" type="text" placeholder="{{ __('COLOR') }}" required="true" aria-required="true" wire:model="color"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">{{ __('MARCA') }}</label>
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <select class="form-control" wire:model="marca">
                                        <option value=''>Seleccione Marca</option>
                                        @foreach($autos as $auto)
                                            <option value={{ $auto->marca }}>{{ $auto->marca }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">{{ __('MODELO') }}</label>
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <select class="form-control" wire:model="car_id">
                                        <option value=''>Seleccione Modelo</option>
                                        @if (count($modelos) > 0)
                                        @foreach($modelos as $modelo)
                                            <option value={{ $modelo->id }}>{{ $modelo->modelo }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">{{ __('PLACA ANTERIOR') }}</label>
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <input class="form-control" name="name" id="input-name" type="text" placeholder="{{ __('PLACA ANTERIOR') }}" aria-required="true" wire:model="placa_anterior"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">{{ __('PLACA VIGENTE') }}</label>
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <input class="form-control" name="name" id="input-name" type="text" placeholder="{{ __('PLACA VIGENTE') }}" aria-required="true" wire:model="placa_vigente"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">{{ __('ESTADO') }}</label>
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <input class="form-control" name="name" id="input-name" type="text" placeholder="{{ __('ESTADO') }}" required="true" aria-required="true" wire:model="estado"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">{{ __('SEDE') }}</label>
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <input class="form-control" name="name" id="input-name" type="text" placeholder="{{ __('SEDE') }}" required="true" aria-required="true" wire:model="sede"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">{{ __('PROPIETARIOS') }}</label>
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <input class="form-control" name="name" id="input-name" type="text" placeholder="{{ __('PROPIETARIOS') }}" required="true" aria-required="true" wire:model="propietario"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">{{ __('TARJETA') }}</label>
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <input class="form-control" name="name" id="input-name" type="text" placeholder="{{ __('TARJETA') }}" required="true" aria-required="true" wire:model="tarjeta"/>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="card-footer ml-auto mr-auto">
                        <button wire:click.prevent="store()" type="button" class="btn btn-primary">{{ __('Guardar') }}</button>
                        <button id="btn_incidencia" data-toggle="modal" data-target="#incidenciaModal" wire:click.prevent="" class="btn btn-info">{{ __('Incidencia') }}</button>
                    </div>
                </div>
            </form>

        </div>
    </div>

    <!-- Modal INCIDENCIA -->
    <div wire:ignore.self class="modal fade" id="incidenciaModal" tabindex="-1" role="dialog" aria-labelledby="incidenciaModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Incidencias</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <input type="hidden" wire:model="num_placa">
                        </div>
                        <div class="form-group">
                            <label>{{ __('Describa la incidencia') }}</label>
                            <textarea class="form-control" rows="4" wire:model="description_incidencia"></textarea>
                        </div>
                    </form>

                    @if ($incidenciaResult)
                    @foreach ($incidenciaResult as $result)
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-text card-header-primary">
                                <div class="card-text">
                                    <h4 class="card-title">{{ \Carbon\Carbon::parse($result['created_at'])->diffForHumans() }}</h4>
                                </div>
                                </div>
                                <div class="card-body">
                                    {{$result['description']}}
                                </div>
                            </div>
                        </div>
                    @endforeach
                    @endif

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" wire:click.prevent="storeModal()" class="btn btn-primary" data-dismiss="modal">Guardar</button>
                </div>
        </div>
        </div>
    </div>

    <script>

        document.addEventListener('livewire:load', function () {
            $("#input_num_placa").on( "click", function() {
                $('#result_num_placa').show(); //muestro mediante id
            });
            $("#btn_num_placa").on( "click", function() {
                $('#result_num_placa').hide(); //oculto mediante id
            });
        })

        /* window.livewire.on('closeModal', () => {
            $('#incidenciaModal').modal('hide');
        }); */
    </script>
</div>
