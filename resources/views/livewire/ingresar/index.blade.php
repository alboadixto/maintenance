@extends('layouts.app', ['activePage' => 'ingresar', 'titlePage' => __('Ingresar')])

@section('content')
  <div class="content">
    <div class="container-fluid">

        <livewire:ingresar.ingresar />

    </div>
  </div>
@endsection
