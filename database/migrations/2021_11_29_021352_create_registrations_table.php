<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registrations', function (Blueprint $table) {
            $table->id();

            $table->string('num_placa', 10);
            $table->string('num_serie', 20);
            $table->string('num_vin', 10);
            $table->string('num_motor', 20);
            $table->string('color', 10);

            $table->unsignedBigInteger('car_id')->nullable();
            $table->foreign('car_id')->references('id')->on('cars')->onDelete('set null')->onUpdate('cascade');

            $table->string('placa_anterior', 10)->nullable();
            $table->string('placa_vigente', 10)->nullable();
            $table->string('estado', 10);
            $table->string('sede', 20);
            $table->string('propietario', 100);
            $table->string('tarjeta', 10);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registrations');
    }
}
